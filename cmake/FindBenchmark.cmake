function(add_benchmark target_name)
  list(REMOVE_AT ARGV 0)
  add_executable(${target_name}
    ${ARGV})

  target_link_libraries(${target_name}
    CONAN_PKG::google-benchmark
    pthread)
endfunction()