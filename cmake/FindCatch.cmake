find_package(Catch2 REQUIRED CONFIG)

add_custom_target(test-all)

add_library(catch_main contrib/catch/catch_main.cpp)

target_link_libraries(catch_main
  CONAN_PKG::Catch2
  CONAN_PKG::glog
  CONAN_PKG::gflags)

function(add_test_binary target_name)
  if (CMAKE_BUILD_TYPE STREQUAL "Coverage")
    add_custom_target(
      run-${target_name}
      WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
      DEPENDS ${target_name}
      COMMAND rm ${target_name}.profraw 2> /dev/null || true
      COMMAND env LLVM_PROFILE_FILE=${target_name}.profraw ./bin/${target_name}
      COMMAND ${LLVM_PROFDATA} merge -sparse ${target_name}.profraw -o ${target_name}.prodata
      COMMAND ${LLVM_COV} report ./bin/${target_name} -instr-profile=${target_name}.prodata)
  else()
    add_custom_target(
      run-${target_name}
      WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
      DEPENDS ${target_name}
      COMMAND ./bin/${target_name})
  endif()

  add_dependencies(test-all run-${target_name})
endfunction()

function(add_catch target_name)
  list(REMOVE_AT ARGV 0)
  add_executable(${target_name}
    ${ARGV})

  target_link_libraries(${target_name}
    catch_main)

  add_test_binary(${target_name})
endfunction()
