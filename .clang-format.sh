#!/usr/bin/sh

find cactus -name '*.h' -or -name '*.cpp' | xargs clang-format -i
