#include <cactus/cactus.h>

enum class KnockProtocol {
    TCP, UDP
};

void KnockPorts(
    const folly::SocketAddress& remote,
    const std::vector<std::pair<int, KnockProtocol>>& ports,
    cactus::Duration delay);