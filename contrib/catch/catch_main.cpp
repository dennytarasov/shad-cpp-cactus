#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

#include <gflags/gflags.h>
#include <glog/logging.h>

int main( int argc, char* argv[] ) {
    gflags::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
    google::InstallFailureSignalHandler();
    FLAGS_logtostderr = true;

    int result = Catch::Session().run( argc, argv );

    // global clean-up...

    return result;
}