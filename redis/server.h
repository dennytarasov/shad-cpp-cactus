#pragma once

#include <folly/SocketAddress.h>

namespace redis {

class Server {
public:
    explicit Server(const folly::SocketAddress& addr, std::unique_ptr<IStorage> storage);

    void Run();

    const folly::SocketAddress& GetAddress() const;
};

}

