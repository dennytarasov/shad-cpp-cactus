#pragma once

#include <stdexcept>

namespace redis {

class RedisError : public std::logic_error {
    using std::logic_error::logic_error;
};

} // namespace redis