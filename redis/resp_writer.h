#pragma once

#include <cactus/io/writer.h>
#include "resp_types.h"

namespace redis {

class RespWriter {
public:
    explicit RespWriter(cactus::IWriter* writer);

    void WriteSimpleString(std::string_view s);
    void WriteError(std::string_view s);
    void WriteInt(int64_t);

    void WriteBulkString(std::string_view s);
    void WriteNullBulkString();

    void StartArray(size_t size);
    void FinishArray();
    void WriteNullArray();
};

}