#include <benchmark/benchmark.h>

#include <cactus/io/writer.h>
#include <redis/resp_writer.h>
#include <redis/resp_reader.h>

constexpr int kNumStrings = 10000;
constexpr int kStringSize = 30000;

void BenchRespReader(benchmark::State& state) {
    cactus::StringWriter writer;
    redis::RespWriter resp_writer(&writer);
    std::string to_write(kStringSize, '$');
    for (int i = 0; i < kNumStrings; ++i) {
        resp_writer.WriteBulkString(to_write);
    }
    auto s = writer.String();
    while (state.KeepRunning()) {
        cactus::BufferReader reader(cactus::View(s));
        redis::RespReader resp_reader(&reader);
        for (int i = 0; i < kNumStrings; ++i) {
            resp_reader.ReadType();
            std::string_view view;
            std::string s;
            if (state.range(0)) {
                s = std::string(*resp_reader.ReadBulkString());
                benchmark::DoNotOptimize(s);
                view = s;
            } else {
                view = *resp_reader.ReadBulkString();
            }
            int sum = 0;
            for (auto c : view) {
                sum += int(c);
            }
            benchmark::DoNotOptimize(sum);
        }
    }
}

BENCHMARK(BenchRespReader)
    ->Unit(benchmark::kMillisecond)
    ->Arg(0)
    ->Arg(1);

BENCHMARK_MAIN();

