#include <catch2/catch.hpp>

#include <thread>
#include <mutex>
#include <utility>

#include <redis/client.h>

#include <cactus/io/reader.h>
#include <cactus/net/net.h>
#include <cactus/core/fiber.h>
#include <cactus/test.h>
#include <redis/simple_storage.h>
#include <redis/server.h>

using namespace cactus;
using namespace redis;

static const auto kServerAddr = folly::SocketAddress(folly::IPAddressV4("127.0.0.1"), 0);

void RunServerCheck(const std::vector<std::pair<std::string, std::string>>& script) {
    auto storage = std::make_unique<SimpleStorage>();
    Server server(kServerAddr, std::move(storage));
    server.Run();

    for (const auto& [req, expected_rsp] : script) {
        auto conn = DialTCP(server.GetAddress());
        conn->Write(cactus::View(req));
        auto rsp = conn->ReadAllToString();
        REQUIRE(rsp == expected_rsp);
    }
}

FIBER_TEST_CASE("Server") {
    SECTION("GetSet") {
        RunServerCheck({
            {
               "*3\r\n" "$3\r\nSET\r\n" "$3\r\nkey\r\n" "$5\r\nvalue\r\n",
               "$2\r\nOK\r\n",
            },
            {
                "*2\r\n" "$3\r\nGET\r\n" "$3\r\nkey\r\n",
                "$5\r\nvalue\r\n",
            },
            {
                "*2\r\n" "$3\r\nGET\r\n" "$7\r\nnot-key\r\n",
                "$-1\r\n",
            }
        });
    }

    SECTION("IncrementDecrement") {
        RunServerCheck({
            {
                "*2\r\n" "$4\r\nINCR\r\n" "$3\r\nkey\r\n",
                ":1\r\n"
            },
            {
                "*2\r\n" "$4\r\nINCR\r\n" "$3\r\nkey\r\n",
                ":2\r\n"
            },
            {
                "*2\r\n" "$4\r\nDECR\r\n" "$3\r\nkey\r\n",
                ":1\r\n"
            },
            {
                "*2\r\n" "$4\r\nDECR\r\n" "$3\r\nkey\r\n",
                ":0\r\n"
            },
            {
                "*2\r\n" "$4\r\nDECR\r\n" "$3\r\nkey\r\n",
                ":-1\r\n"
            },
        });
    }
}