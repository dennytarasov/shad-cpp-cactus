#include <catch2/catch.hpp>

#include <redis/resp_writer.h>

#include <cactus/io/writer.h>

using namespace cactus;
using namespace redis;

TEST_CASE("RESP writer") {
    SECTION("SimpleString") {
        StringWriter writer;
        RespWriter resp_writer(&writer);
        resp_writer.WriteSimpleString("Simple string");
        REQUIRE(writer.String() == "+Simple string\r\n");
    }

    SECTION("Error") {
        StringWriter writer;
        RespWriter resp_writer(&writer);
        resp_writer.WriteError("ERR Some error");
        REQUIRE(writer.String() == "-ERR Some error\r\n");
    }

    SECTION("Int") {
        StringWriter writer;
        RespWriter resp_writer(&writer);
        resp_writer.WriteInt(-1099);
        REQUIRE(writer.String() == ":-1099\r\n");
    }

    SECTION("BulkString") {
        StringWriter writer;
        RespWriter resp_writer(&writer);
        resp_writer.WriteBulkString("Bulk\r\nstring");
        REQUIRE(writer.String() == "$12\r\nBulk\r\nstring\r\n");
    }

    SECTION("Null BulkString") {
        StringWriter writer;
        RespWriter resp_writer(&writer);
        resp_writer.WriteNullBulkString();
        REQUIRE(writer.String() == "$-1\r\n");
    }

    SECTION("Array") {
        StringWriter writer;
        RespWriter resp_writer(&writer);
        resp_writer.StartArray(3);
        resp_writer.WriteSimpleString("blah");
        resp_writer.WriteInt(90);
        resp_writer.WriteError("ERR 404");
        REQUIRE(writer.String() ==
                "*3\r\n"
                "+blah\r\n"
                ":90\r\n"
                "-ERR 404\r\n");
    }

    SECTION("Null array") {
        StringWriter writer;
        RespWriter resp_writer(&writer);
        resp_writer.WriteNullArray();
        REQUIRE(writer.String() == "*-1\r\n");
    }
}