#!/bin/bash

set -x

git checkout master -- \
    cactus \
    cmake \
    README.md \
    CMakeLists.txt \
    .gitlab-ci.yml \
    portknock \
    portscan \
    socks4 \
    fanout \
    conanfile.txt \
    redis \
    socksd \
    balancer

tee {portknock,portscan,socks4,fanout,socksd}/main.cpp <<EOF
int main(int argc, char* argv[]) {
    return 0;
}
EOF

tee < /dev/null \
    portknock/portknock.cpp \
    portscan/portscan.cpp \
    socks4/socks4.cpp \
    cactus/rpc/rpc.cpp \
    socksd/socksd.cpp \
    balancer/balancer.cpp

mv cactus/rpc/gen-rpc/rpc_generator{_example.cpp,.cpp}

for f in resp_reader resp_writer client server simple_storage; do
    tee < /dev/null redis/$f.cpp
    mv redis/$f\_public.h redis/$f.h
done

git add .
