#include "address.h"

#include <string.h>
#include <netinet/in.h>
//#include <ares.h>

#include <boost/core/noncopyable.hpp>

namespace cactus {

class CAresResolver : public boost::noncopyable {
public:
    CAresResolver() {
        //        CHECK(ares_init(&chan_) == ARES_SUCCESS);
    }

    ~CAresResolver() {
        //       ares_destroy(chan_);
    }

    virtual folly::SocketAddress Resolve(std::string_view address) {
        CHECK(false) << "Not implemented";
    }

private:
    // ares_channel chan_;
};

}  // namespace cactus
