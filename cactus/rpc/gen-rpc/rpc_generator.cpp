#include "rpc_generator.h"

bool RpcGenerator::Generate(const google::protobuf::FileDescriptor* file,
                            const std::string& parameter,
                            google::protobuf::compiler::GeneratorContext* generator_context,
                            std::string* error) const {
    auto name = file->name();
    // Cut ".pb.cc"
    name = name.substr(0, name.size() - 6);

    std::unique_ptr <google::protobuf::io::ZeroCopyOutputStream> out_h_includes(
        generator_context->OpenForInsert(name + ".pb.h", "includes"));
    google::protobuf::io::Printer pb_includes(out_h_includes.get(), '$');
    pb_includes.Print("#include <cactus/rpc/rpc.h>\n");
    pb_includes.Print("#include <memory>\n");

    std::unique_ptr <google::protobuf::io::ZeroCopyOutputStream> out_h(
        generator_context->OpenForInsert(name + ".pb.h", "namespace_scope"));
    google::protobuf::io::Printer pb_h(out_h.get(), '$');

    for (int i = 0; i < file->service_count(); ++i) {
        auto service = file->service(i);

        std::map <std::string, std::string> vars;
        vars["service_name"] = service->name();

        pb_h.Print(vars, "class $service_name$Client {\n");
        pb_h.Print("public:\n");
        pb_h.Indent();

        pb_h.Print(
            vars,
            "$service_name$Client(::cactus::IRpcChannel* channel) : channel_(channel) {}\n");

        // YOUR CODE GOES HERE...

        pb_h.Outdent();
        pb_h.Print("};\n");
    }

    std::unique_ptr <google::protobuf::io::ZeroCopyOutputStream> out_cc(
        generator_context->OpenForInsert(name + ".pb.cc", "namespace_scope"));
    google::protobuf::io::Printer pb_cc(out_cc.get(), '$');

    // ... AND HERE

    return true;
}