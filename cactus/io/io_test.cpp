#include <catch2/catch.hpp>

#include <cactus/io/writer.h>
#include <cactus/io/reader.h>

using namespace cactus;

TEST_CASE("Formatting") {
    StringWriter w;

    w.Format("{} {} {}", 1, 1.25, "abc");
    REQUIRE("1 1.25 abc" == w.String());
}

TEST_CASE("BufferedReader") {
    std::string input = "foo\nbar\nzog";
    ViewReader r(View(input));

    REQUIRE("foo\n" == r.ReadString('\n'));
    REQUIRE("ba" == r.ReadString('\n', 2));
    REQUIRE("r\n" == r.ReadString('\n'));
    REQUIRE("zog" == r.ReadString('\n'));
    REQUIRE(r.ReadString('\n').empty());
}