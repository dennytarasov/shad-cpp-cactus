add_library(libsocksd socksd.cpp)
target_link_libraries(libsocksd cactus)

add_catch(test_socksd socksd_test.cpp)
target_link_libraries(test_socksd libsocksd)

add_executable(socksd main.cpp)
target_link_libraries(socksd libsocksd)